# Bus Pirate ASCII to binary 

Converts an ASCII capture file of a Bus Pirate I2C dump captured by a serial terminal emulator to a binary file. 
Specifically: it converts the hex values in the lines starting with **READ:** to binary numbers and saves them to a new file.
## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

What things you need to install before you can run my program:

```
Python 2.7
```

### Installing

Quite simply; Down load this python file to wherever you like on you machine.

### Running It
If you have a capture file named myCapture.txt here is how you would convert it to a binary file of the data.

```
python bp2bin.py myCapture.txt
```
This will convert the hex codes found in the capture file to binary and save to a file named myCapture.txt.bin

### Details
A capture file like this will be parsed and the hex values in the lines starting with **READ:** will be converted to binary and saved to a binary file.

```
HiZ>m

1. HiZ
2. 1-WIRE
3. UART
4. I2C
5. SPI
6. 2WIRE
7. 3WIRE
8. LCD
9. DIO
x. exit(without change)

(1)>4
Set speed:
 1. ~5KHz
 2. ~50KHz
 3. ~100KHz
 4. ~400KHz

(1)>2
Ready
I2C>W
Power supplies ON
I2C>(1)
Searching I2C address space. Found devices at:
0xA0(0x50 W) 0xA1(0x50 R) 

I2C>[0xa0 0x00 0x00 [0xa1 r:8]
I2C START BIT
WRITE: 0xA0 ACK 
WRITE: 0x00 ACK 
WRITE: 0x00 ACK 
I2C START BIT
WRITE: 0xA1 ACK 
READ: 0x80  ACK 0x0F  ACK 0x00  ACK 0xCC  ACK 0x88  ACK 0x13  ACK 0xFF  ACK 0x00
NACK
I2C STOP BIT
I2C>[0xa1 r:8]
I2C START BIT
WRITE: 0xA1 ACK 
READ: 0x0A  ACK 0x00  ACK 0x21  ACK 0x00  ACK 0x06  ACK 0x20  ACK 0x41  ACK 0x40
NACK
I2C STOP BIT

```
Enjoy!
