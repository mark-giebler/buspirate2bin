#!/usr/bin/python
# ==========================================================================
# Mark Giebler
# 2019-02-23
# bp2bin.py
#
# translates bus pirate I2C ascii dumps capture in a serial terminal emulator
# to binary format and saves to a bin file.
#
# Usage:
#   bp2bin <bp_dump_name>
# Output file will be created with name: <bp_dump_name>.bin
# =========================================================================
# ==========================================================================
# IMPORTS
# ==========================================================================
from __future__ import division, with_statement, print_function
import sys
import os
import time
import datetime

#from aardvark_py import *

# ==========================================================================
# CONSTANTS
# ==========================================================================
VERSION = "1.00"


# ==========================================================================
# FUNCTIONS
# ==========================================================================

def _convertLine(line, outFileH):
	byten = 0  # type: int
	if line[0:6] == 'READ: ':
		datatokens = line[6:]
		tokens = datatokens.split()
#		print("tk len:  %s" %  str(len(tokens)))
		for n in tokens:
			assert isinstance(n, object)
			if n[0:3] != 'ACK':
				c = int(n, 16)
				outFileH.write(bytearray([c]))
				byten += 1
#				print(n)
#			else:
#				print("SKIP: " + n)
		print("wrote: %d bytes" % byten)
	return byten

# ==========================================================================
# MAIN PROGRAM
# ==========================================================================
print("Version: %s" % str(VERSION))
if len(sys.argv) < 2:
	print("Error: Too few arguments.")
	print("usage: ")
	print("       bp2bin.py bp-input_name\n")
	print("where: ")
	print("       bp-input_name - filepath to ascii capture of Bus Pirate output")
	sys.exit()

filepath = sys.argv[1]

if not os.path.isfile(filepath):
	print("ERROR: File {} does not exist.".format(filepath))
	sys.exit()

bytecount=0 # type: int
outfile = filepath+'.bin'
with open(outfile, 'w+b') as outfp:
	with open(filepath) as fp:
		cnt = 0
		for line in fp:
#			print("Line {}: {}".format(cnt, line.strip()))
			bytecount += _convertLine(line, outfp)
			cnt += 1
outfp.close()
print("Created binary file: "+outfile)
print("Length: %d bytes" % bytecount)
